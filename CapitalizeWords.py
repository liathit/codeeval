with open('CapitalizeWords.txt', 'r') as test_cases:
    for word in list(test_cases):
        print ' '.join(i[0].capitalize() + i[1:] for i in word.split())