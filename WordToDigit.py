import sys

digits = { 'zero': '0', 'one': '1', 'two': '2', 'three': '3', 'four': '4',
'five': '5', 'six': '6', 'seven': '7', 'eight': '8', 'nine': '9'}

with open('WordToDigit.txt', 'r') as test_case:
    for test in test_case:
        line = test.strip().split(';')
        translation = []
        for x in line:
            print digits[x]
            translation.append(digits[x])
            print translation
        print ''.join(translation)
    test_case.close()
