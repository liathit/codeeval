#!/usr/bin/python2.7

import numbers

def filter_list(l):
    return filter(lambda x: isinstance(x, numbers.Number), l)

print(filter_list([1,2,'a','b']))
print(filter_list([1,2,'aasf','1','123',123]))
