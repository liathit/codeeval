import string

def is_pangram(s):
   letter = list(string.ascii_lowercase)
   return [i for i in letter if i in list(s.lower())] == letter

print is_pangram('The quick, brown fox jumps over the lazy dog!')
