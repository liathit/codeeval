def diamond(n):
    middle = '*' * n
    top_bottom = '*' * (n // 2)
    diamond = [top_bottom, middle, top_bottom]
    return '\n'.join(diamond)

print diamond(3)

'''
 *
***
 *
'''
