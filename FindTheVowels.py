def vowel_indices(word):
    up = word.lower()
    vowels = list('aeiouy')
    return [w for w in up if w in vowels]

'''
for w in word:
        if w in vowels:
            print word.index(w) + 1
'''
