from math import sqrt

with open('CalculateDistance.txt', 'r') as test_cases:
    for test in test_cases:
        line = test.rstrip()
        if line != '':
            tr = line.translate(string.maketrans("", ""), '(),')
            cord = tr.split()
            distance = sqrt((int(cord[0]) - int(cord[2])) ** 2 + (int(cord[1]) - int(cord[3])) ** 2)
            print int(distance)
