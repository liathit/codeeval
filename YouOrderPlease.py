'''
Your task is to sort a given string. Each word in the String will
contain a single number. This number is the position the word should
have in the result.

For an input: "is2 Thi1s T4est 3a" the function should return "Thi1s
If the input String is empty, return an empty String. The words in the
input String will only contain valid consecutive numbers.

Note: Numbers can be from 1 to 9. So 1 will be the first word (not 0). is2 Thi1s 3a T4est"
'''

s = 'is2 Thi1s T4est 3a'

#My first solution
'''
import re

def order(sentence):
   out = []
   n = re.findall('\d+', sentence)
   words = sentence.split()
   pack = zip(n, words)
   pack.sort()
   for i in pack:
      out.append(i[1])
   return ' '.join(out)
'''

'''
for i in l:                                                                 
  for d in i:                                                             
    if d.isdigit():                                                 
      print i
'''

'''
def order(sentence):
    return " ".join(sorted(sentence.split(), key=lambda x: int(filter(str.isdigit, x))))
'''
def order(words):
  return ' '.join(sorted(words.split(), key=lambda w:sorted(w)))
