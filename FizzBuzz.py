import sys

with open('FizzBuzz.txt', 'r') as stream:
    for test in stream:
        x = int(test.split(' ')[0])
        y = int(test.split(' ')[1])
        n = int(test.split(' ')[2])

        for i in range(1, n+1):
            if (i % x == 0) and (i % y == 0):
                sys.stdout.write('FB' + ' ')
            elif i % x == 0:
                sys.stdout.write('F' + ' ')
            elif i % y == 0:
                sys.stdout.write('B' + ' ')
            else:
                sys.stdout.write(str(i) + ' ')
        print
    stream.close()